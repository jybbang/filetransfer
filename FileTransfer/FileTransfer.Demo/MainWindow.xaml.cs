﻿using FileTransfer.Core;
using MvvmCross.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FileTransfer.Demo
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly RedisHelper redis;

        public string Host { get; set; }
        public bool Conn { get; set; }

        public string TargetPath { get; set; }
        public string DestDir { get; set; }

        public long SendLatency { get; set; }
        public long DestLatency { get; set; }

        public long SendSize { get; set; }
        public long DestSize { get; set; }

        public string TargetMD5 { get; set; }
        public string DestMD5 { get; set; }

        public bool IsSame { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            redis = new RedisHelper();
        }

        public MvxCommand CONNECT => new MvxCommand(() =>
        {
            Conn = redis.Connect(Host);
        });

        public MvxAsyncCommand SEND => new MvxAsyncCommand(async () =>
        {
            using (var d = new OpenFileDialog())
            {
                if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    TargetPath = d.FileName;
                    SendLatency = await redis.SendFileAsync("F", TargetPath);
                    var fInfo = new FileInfo(TargetPath);
                    SendSize = fInfo.Length;
                    TargetMD5 = redis.GetMD5HashFromFile(TargetPath);
                }
            }
        }, () => Conn);

        public MvxAsyncCommand OPEN => new MvxAsyncCommand(async () =>
        {
            using (var d = new FolderBrowserDialog())
            {
                if (d.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    DestDir = d.SelectedPath;
                    var n = redis.GetNewPath(TargetPath, DestDir);
                    DestLatency = await redis.GetFileAsync("F", TargetPath, n);
                    DestMD5 = redis.GetMD5HashFromFile(n);
                    var fInfo = new FileInfo(n);
                    DestSize = fInfo.Length;
                    IsSame = TargetMD5.Equals(DestMD5);
                }
            }
        }, () => Conn);
    }
}
