﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileTransfer.Core
{
    public class RedisHelper
    {
        private ConnectionMultiplexer conn;

        public IDatabase Db { get; private set; }
        public IServer Server { get; private set; }
        public ISubscriber Sub { get; private set; }

        public bool Connect(string host, string pwd = "", int port = 6379)
        {
            try
            {
                var config = new ConfigurationOptions();
                config.EndPoints.Add(host, port);
                config.Password = pwd;
                config.AbortOnConnectFail = true;
                conn = ConnectionMultiplexer.Connect(config);

                if (conn.IsConnected)
                {
                    Server = conn.GetServer(conn.GetEndPoints()[0]);
                    Db = conn.GetDatabase();
                    Sub = conn.GetSubscriber();
                }
                return conn.IsConnected;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

            return false;
        }

        public long SetFile(string key, string path)
        {
            var sw = new Stopwatch();
            sw.Start();

            byte[] result = File.ReadAllBytes(path);
            var str = Convert.ToBase64String(result);
            Db.HashSet(key, path, str);

            sw.Stop();
            return sw.ElapsedMilliseconds;
        }

        public async Task<long> SendFileAsync(string key, string path)
        {
            var sw = new Stopwatch();
            sw.Start();

            byte[] result;
            using (FileStream stream = File.Open(path, FileMode.Open))
            {
                result = new byte[stream.Length];
                await stream.ReadAsync(result, 0, (int)stream.Length);
            }

            var str = Convert.ToBase64String(result);
            await Db.HashSetAsync(key, path, str);

            sw.Stop();
            return sw.ElapsedMilliseconds;
        }

        public long GetFile(string key, string path, string newPath)
        {
            var sw = new Stopwatch();
            sw.Start();

            var str = Db.HashGet(key, path);
            var result = Convert.FromBase64String(str);
            File.WriteAllBytes(newPath, result);

            sw.Stop();
            return sw.ElapsedMilliseconds;
        }

        public async Task<long> GetFileAsync(string key, string path, string newPath)
        {
            var sw = new Stopwatch();
            sw.Start();


            string str = await Db.HashGetAsync(key, path);
            var result = Convert.FromBase64String(str);
            using (FileStream sourceStream = new FileStream(newPath,
                FileMode.CreateNew, FileAccess.Write, FileShare.None,
                bufferSize: 4096, useAsync: true))
            {
                await sourceStream.WriteAsync(result, 0, result.Length);
            };

            sw.Stop();
            return sw.ElapsedMilliseconds;
        }

        public int ToInt(IEnumerable<char> s)
        {
            if (s == null) return 0;
            var t = from c in s let i = c - '0' where (i >= 0 && i <= 9) || c == '-' select c;
            if (int.TryParse(string.Concat(t), out int ret)) return ret;
            return 0;
        }

        public string GetNewPath(string path, string targetDir)
        {
            var fname = Path.GetFileNameWithoutExtension(path);
            var fext = Path.GetExtension(path);

            var serial = string.Empty;
            var newPath = string.Empty;
            do
            {
                newPath = Path.GetFullPath($@"{targetDir}/{fname}{serial}{fext}");
                serial = $"_{ToInt(serial) + 1}";
            }
            while (File.Exists(newPath));

            return newPath;
        }

        public string GetMD5HashFromFile(string fileName)
        {
            using (FileStream file = new FileStream(fileName, FileMode.Open))
            {
                System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] hash = md5.ComputeHash(file);

                StringBuilder result = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    result.Append(hash[i].ToString("x2"));
                }
                return result.ToString();
            }
        }
    }
}
